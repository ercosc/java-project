package listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controler.ControlManager;
import view.MainWindow;

public class BtnAdicionarProdutoListener implements ActionListener {
	private MainWindow mw;
	private ControlManager cm;

	public BtnAdicionarProdutoListener(MainWindow mw, ControlManager cm) {
		this.setMw(mw);
		this.setCm(cm);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.getMw().getFrame().setVisible(false);
		this.getCm().newProductWindow();
	}
	
	public MainWindow getMw() {
		return this.mw;
	}
	public void setMw(MainWindow mw) {
		this.mw = mw;
	}

	public ControlManager getCm() {
		return this.cm;
	}
	public void setCm(ControlManager cm) {
		this.cm = cm;
	}
}
