package model;

import java.util.ArrayList;

public class Inventory {
	private ArrayList<Departament> departaments;
	private String nome;
	
	public ArrayList<Departament> getDepartaments() {
		return departaments;
	}
	public void setDepartaments(ArrayList<Departament> departaments) {
		this.departaments = departaments;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
		
}
