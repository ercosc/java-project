package controler;

import view.MainWindow;

public class ControlManager {
	private DepartamentControl dc;
	private InventoryControl ic;
	private ProductControl pc;
	private UserControl uc;
	
	private MainWindow mw;
	
	 public ControlManager() {
		 this.setDc(new DepartamentControl());
		 this.setIc(new InventoryControl());
		 this.setPc(new ProductControl());
		 this.setUc(new UserControl());
		 this.setMw(new MainWindow(this));
	 }

	public DepartamentControl getDc() {
		return dc;
	}
	public void setDc(DepartamentControl dc) {
		this.dc = dc;
	}

	public InventoryControl getIc() {
		return ic;
	}
	public void setIc(InventoryControl ic) {
		this.ic = ic;
	}

	public ProductControl getPc() {
		return pc;
	}
	public void setPc(ProductControl pc) {
		this.pc = pc;
	}

	public UserControl getUc() {
		return uc;
	}
	public void setUc(UserControl uc) {
		this.uc = uc;
	}

	
	public MainWindow getMw() {
		return mw;
	}

	public void setMw(MainWindow mw) {
		this.mw = mw;
	}

	public void newProductWindow() {
		this.getPc().newProductWindow();
	}
}
