package view;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import controler.ControlManager;
import listener.BtnAdicionarProdutoListener;

public class MainWindow {

	private JFrame frame;
	private ControlManager cm;

	public MainWindow(ControlManager cm) {
		this.setCm(cm);
		initialize(this.getCm());
		
	}

	private void initialize(ControlManager cm) {
		frame = new JFrame();
		frame.setBounds(100, 100, 240, 220);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		frame.setResizable(false);	
		
		JButton btnAdicionarProduto = new JButton("adicionar produto");
		btnAdicionarProduto.addActionListener(new BtnAdicionarProdutoListener(this, cm));
		btnAdicionarProduto.setBounds(43, 65, 150, 23);
		frame.getContentPane().add(btnAdicionarProduto);
		
		JButton btnNewButton = new JButton("Remover Produto");
		btnNewButton.setBounds(43, 99, 150, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Listar produtos");
		btnNewButton_1.setBounds(43, 133, 150, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblControleDeEstoque = new JLabel("Controle de Estoque");
		lblControleDeEstoque.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblControleDeEstoque.setBounds(43, 11, 150, 30);
		frame.getContentPane().add(lblControleDeEstoque);
		
	}

	public JFrame getFrame() {
		return frame;
	}
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public ControlManager getCm() {
		return this.cm;
	}
	public void setCm(ControlManager cm) {
		this.cm = cm;
	}
}
